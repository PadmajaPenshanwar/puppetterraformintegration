provider "aws" {
  region = "us-east-1"
}

# Include the puppet resources from separate files
# resource "aws_instance" "puppet_master" {
#   ami           = "ami-07d9b9ddc6cd8dd30"
#   instance_type = "t2.micro"

#   # Other instance configuration settings for Puppet Master
#   key_name      = "myec2key"
#   vpc_security_group_ids = ["sg-0dfa915c7c0bd195e"]

#   user_data = file("./master_install.sh")

#   tags = {
#     Name = "puppet-master"
#   }
# }

resource "aws_instance" "puppet_slave" {
  ami           = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"

  # Other instance configuration settings for Puppet Master
  key_name      = "myec2key"
  vpc_security_group_ids = ["sg-0dfa915c7c0bd195e"]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("C:/Users/lenovo/Downloads/myec2key.pem")
    host        = self.public_ip
    timeout     = "20m" # Adjust timeout as per your requirement
  }


  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo wget https://apt.puppetlabs.com/puppet-release-bionic.deb",
      "sudo dpkg -i puppet-release-bionic.deb",
      "sudo apt-get update",
      "sudo apt install puppet -y",
      "sudo sh -c 'echo \"54.196.148.135 puppet\" >> /etc/hosts'",
      "sudo systemctl start puppet",
      "sudo systemctl enable puppet",
    ]
  }

  tags = {
    Name = "puppet_slave"
  }
}